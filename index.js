const express = require('express');
const app = express();
const port = 3000;
const request = require('request');
var faker = require('faker');

// Ссылку куда редиректим если вдруг что-то не так с токеном
const loginUrl = 'http://localhost:3000';
//
const appUrl = 'https://dev.app.mybase.pro';
// Api mybase
const apiEndpoint = 'https://dev.api.mybase.pro';
// Тестируем авторизацию с ранее полученным токеном
const testRegisteredToken = '284N6N27XFLko4b3z7s44Ahzl9FbUnnd';


app.get('/', (req, res) => {
  const email = faker.internet.email();
  const name = faker.name.findName();

  const template = `
  <h1>Регистрация нового пользователя:</h1>
  <div>Почта: ${email}</div>
  <div>Имя: ${name}</div>
  <a href="/sign-up?email=${email}&name=${name}">Зарегистрировать</a>
  <hr>
  <h1>Авторизация ранее зарегистрированного пользователя:</h1>
  <div>Токен: ${testRegisteredToken}</div>
  <a href="/sign-in">Авторизовать</a>
  `;
    res.send(template)
});

app.get('/sign-up', (req, res) => {
    const name = req.query.name;
    const email = req.query.email;
    request.post(`${apiEndpoint}/innmind/authentications/sign-up`, {
        form: {
            email: email,
            firstName: name,
            floor: 'man' // or woman
        }
    }, (err, result, body) => {
        body = JSON.parse(body);
        if (result.statusCode === 422) {
            // Validation error
            res.send(body)
        } else if (result.statusCode === 200) {
            const accessToken = body.token;
            res.redirect(`${appUrl}/auth/sign-in/${accessToken}?loginUrl=${loginUrl}`)
        }
    });
});

app.get('/sign-in', (req, res) => {
    res.redirect(`${appUrl}/auth/sign-in/${testRegisteredToken}?loginUrl=${loginUrl}`)
});


app.listen(port, () => {
    console.log(`Innmind app listening at http://localhost:${port}`)
});
